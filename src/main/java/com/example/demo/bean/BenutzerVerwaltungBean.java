package com.example.demo.bean;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class BenutzerVerwaltungBean {

    private static final Logger LOG = LoggerFactory.getLogger(BenutzerVerwaltungBean.class);

    private List<User> users;

    private final UserService userService;

    public BenutzerVerwaltungBean(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    public void load() {
        LOG.info("loading users");
        users = userService.getAllUsers();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
