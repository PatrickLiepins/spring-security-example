package com.example.demo;

import com.example.demo.entity.User;
import com.example.demo.entity.UserGroup;
import com.example.demo.entity.UserRole;
import com.example.demo.repository.UserGroupRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UserRoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class Initializer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserGroupRepository userGroupRepository;
    private final PasswordEncoder passwordEncoder;

    public Initializer(UserRepository userRepository, UserRoleRepository userRoleRepository, UserGroupRepository userGroupRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.userGroupRepository = userGroupRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        UserRole roleBenutzerverwaltungLesen = new UserRole("ROLE_BENUTZERVERWALTUNG_LESEN");
        userRoleRepository.save(roleBenutzerverwaltungLesen);

        UserRole roleModeratorLesen = new UserRole("ROLE_MODERATOR_LESEN");
        userRoleRepository.save(roleModeratorLesen);

        UserGroup moderator = new UserGroup("MODERATOR");
        moderator.getUserRoles().add(roleModeratorLesen);
        userGroupRepository.save(moderator);

        UserGroup admin = new UserGroup("ADMIN");
        admin.getUserRoles().add(roleModeratorLesen);
        admin.getUserRoles().add(roleBenutzerverwaltungLesen);
        userGroupRepository.save(admin);


        User user = new User("user", passwordEncoder.encode("user"));
        userRepository.save(user);

        User moderatorUser = new User("moderator", passwordEncoder.encode("moderator"));
        moderatorUser.getUserGroups().add(moderator);
        userRepository.save(moderatorUser);

        User adminUser = new User("admin", passwordEncoder.encode("admin"));
        adminUser.getUserGroups().add(admin);
        userRepository.save(adminUser);


//            User mod = new User();
//            mod.setUsername("mod");
//            mod.setPassword(passwordEncoder.encode("mod"));
//
//            UserGroup userGroup = new UserGroup();
//            userGroup.setName("MODERATOR");
//
//            userGroup.getUserRoles().add(modRole);
//            userGroupRepository.save(userGroup);
//
//            mod.getUserGroups().add(userGroup);
//            userRepository.save(mod);
    }
}
