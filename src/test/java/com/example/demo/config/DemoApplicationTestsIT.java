package com.example.demo.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PersistenceConfigTest.class)
public class DemoApplicationTestsIT {

    @Test
    public void contextLoads() {
        System.out.println("=======================================");
    }

}
